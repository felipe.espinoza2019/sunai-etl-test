Prueba de Selección de Personal (PSP)

Requisitos:

- ​	Tener instalado Python en la versión 3.7 o superior
- ​	Tener una cuenta de Gitlab y (opcional) programa para subir resultados (ej Sublime Merge), en caso de no tener conocimientos de Git indicar con anticipación.
- ​	(opcional) IDE VSCode o equivalente

Problema:

Hoy tendrás que hacer un caso típico de conexión a una API que implica llamadas  cada 5 minutos para traer la data actualizada del inversor para mostrarlo en otra plataforma, y una llamada 1 vez al día para traer toda la data del día para asegurarnos que la data de los llamados del día si esten en la base de datos, por ello debes crear un modulo (o mas) para llamar a los inversores de la marca *sunapi* y para el inversor con serial number *01110011* (la respuesta de la API corresponde a este inversor), cada llamado cuesta $0.01 USD además debe ser subido a la base de datos PostgreSQL.

Ademas, se agrego a la base de datos la planta a la tabla "Planta" para obtener los datos necesarios para acceder a la API con la planta "Estacionamiento" con la apikey "Sunai".

URL de api: http://3.144.48.134/docs#/

Instrucciones adicionales

Se debe considerar este proyecto como si ya estuvieras trabajando en el equipo para que tomes las mejores practicas y desiciones con el codigo. El script debe ser añadido al proyecto actual que se le dará acceso, el cual deberá hacerle un fork y crear una rama llamada "apiSunapi", desde esa rama al momento de hacer push ir a la pagina y pedir merge **sin embargo no aceptarlo y solo crear la petición**, posteriormente compartir el repositorio a **@jua.retamales**.

Además, para efectos del desarrollo se debe aplicar la query llamada db.sql que creara la estructura necesaria para el desarrollo, por recomendación puede usar https://www.elephantsql.com/ debido a que deberá adjuntar el acceso a esta para verificar la inserción.

