import datetime as dt
# from dateutil.relativedelta import relativedelta
import argparse
# import logging
# import os
import pandas as pd
import numpy as np
# import pytz


########################
# MAIN CODE ############ 
########################

def main(args):
    # insertar codigo aqui
    


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Read inverters values and push data to Neural')

    parser.add_argument('-host', action='store', dest='HOST',
                        help='IP of db host')
    parser.add_argument('-usr', action='store', dest='USER',
                        help='Username of db')
    parser.add_argument('-pwd', action='store', dest='PASSWORD',
                        help='Password of db')
    parser.add_argument('-prt', action='store', dest='PORT',
                        help='Port of db host')
    parser.add_argument('-db', action='store', dest='DATABASE',
                        help='Database name')
    parser.add_argument('-id_planta', action='store', dest='ID_PLANTA',
                        help='Identificador Planta')

    args = parser.parse_args()

    main(args)